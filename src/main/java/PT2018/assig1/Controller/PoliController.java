package PT2018.assig1.Controller;

import PT2018.assig1.Model.MonomModel;
import PT2018.assig1.Model.PolinomModel;
import PT2018.assig1.View.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class PoliController {

    private PoliView theView;
    private PolinomModel theModelp1;
    private PolinomModel theModelp2;
    private List<MonomModel> listaux;
    private List<MonomModel> listaux2;

    /**
     * Constructor for initializing all buttons and textfields
     * @param theModelp1 the first polynomial
     * @param theModelp2 the first polynomial
     * @param theView class for gui
     */
    public PoliController (PolinomModel theModelp1, PolinomModel theModelp2, PoliView theView){
        this.theModelp1=  theModelp1;
        this.theModelp2=  theModelp2;
        this.theView = theView;

        this.theView.addDerivateListener(new DerivateListener());
        this.theView.addIntegrateListener(new IntegrateListener());
        this.theView.addMulListener(new MultiListener());
        this.theView.addDivListener(new DiviListener());
        this.theView.addSumListener(new SumListener());
        this.theView.addDiferentaListener(new DiferentaListener());

    }

    /**
     * Class to implemet the actoin listener for differentiating
     */
    class DerivateListener implements ActionListener{

        public void actionPerformed(ActionEvent arg0){
            String firstPoli= null;
            String aux;
            String aux1 = "";

                firstPoli = theView.getFisrtPoli();
                theModelp1= new PolinomModel(firstPoli);
             aux = theModelp1.makeString(theModelp1.derivarePol());
             for(int i = 0; i < aux.length(); i++)
             {
                 if(i != aux.length() - 1 && aux.charAt(i) != '+')
                 {
                     aux1+= aux.charAt(i);
                 }
             }

             if(aux1.length()==0){
                 theView.setCalculateSol("0");
             }
             else{
            theView.setCalculateSol(aux1);
            theView.setRemainderSol(" ");
            }
        }
    }

    /**
     * Class to implemet the actoin listener for integrating
     */
    class IntegrateListener implements ActionListener{

        public void actionPerformed(ActionEvent arg0) {
            String firtsPoli = null;
            firtsPoli = theView.getFisrtPoli();
            theModelp1 = new PolinomModel(firtsPoli);

            theView.setCalculateSol(theModelp1.makeString(theModelp1.integrarePol()));
            theView.setRemainderSol(" ");

        }
    }

    /**
     * Class to implemet the actoin listener for multiplying
     */
    class MultiListener implements ActionListener{
        public void actionPerformed (ActionEvent arg0){
            String firtsPoli, secondPoli = null;
            secondPoli = theView.getSecondPoli();
            firtsPoli = theView.getFisrtPoli();
            theModelp1 = new PolinomModel(firtsPoli);
            theModelp2 = new PolinomModel(secondPoli);
            String aux1;
            aux1=theModelp1.makeString(theModelp1.mul(theModelp2));

            if(aux1.length()==0){
                theView.setCalculateSol("0");
            }
            else{
                theView.setCalculateSol(aux1);
                theView.setRemainderSol(" ");
            }

        }
    }

    /**
     * Class to implemet the actoin listener for dividing
     */
    class  DiviListener implements ActionListener{
        public void actionPerformed (ActionEvent arg0){
            String firtsPoli, secondPoli = null;
            secondPoli = theView.getSecondPoli();
            firtsPoli = theView.getFisrtPoli();
            theModelp1 = new PolinomModel(firtsPoli);
            theModelp2 = new PolinomModel(secondPoli);

           // theView.setCalculateSol(theModelp1.makeString(theModelp1.division(theModelp2)));

            String s1 = "";
            String s2 = "";
            theModelp1.setMyList(theModelp1.division(theModelp2));
            int ii= theModelp1.getIndexRez();
            listaux=theModelp1.geMtList().subList(0, ii);
            s1=theModelp1.makeString(listaux);
            listaux2=theModelp1.geMtList().subList(ii,theModelp1.getSizee() );
            s2=theModelp1.makeString(listaux2);

            if(s2.length()==0){
                theView.setCalculateSol("0");
            }
            else{
                theView.setCalculateSol(s2);
                theView.setRemainderSol(s1);
            }


        }
    }

    /**
     * Class to implemet the actoin listener for addition
     */
    class  SumListener implements ActionListener{
        public void actionPerformed (ActionEvent arg0){
            String firtsPoli, secondPoli = null;
            secondPoli = theView.getSecondPoli();
            firtsPoli = theView.getFisrtPoli();
            theModelp1 = new PolinomModel(firtsPoli);
            theModelp2 = new PolinomModel(secondPoli);

            String aux1;
            aux1=theModelp1.makeString(theModelp1.sum(theModelp2));

            if(aux1.length()==0){
                theView.setCalculateSol("0");
            }
            else{
                theView.setCalculateSol(aux1);
                theView.setRemainderSol(" ");
            }


        }
    }

    /**
     * Class to implemet the actoin listener for substraction
     */
    class  DiferentaListener implements ActionListener{
        public void actionPerformed (ActionEvent arg0){
            String firtsPoli, secondPoli = null;
            secondPoli = theView.getSecondPoli();
            firtsPoli = theView.getFisrtPoli();
            theModelp1 = new PolinomModel(firtsPoli);
            theModelp2 = new PolinomModel(secondPoli);

            String aux1;
            aux1=theModelp1.makeString(theModelp1.diferenta(theModelp2));

            if(aux1.length()==0){
                theView.setCalculateSol("0");
            }
            else{
                theView.setCalculateSol(aux1);
                theView.setRemainderSol(" ");
            }


        }
    }
}
