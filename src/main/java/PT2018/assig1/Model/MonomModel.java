package PT2018.assig1.Model;

public class MonomModel {

   private double coeff;
    private int degree;


    /**
     * Contructor for making a monom
     * @param coeff
     * @param degree
     */
    MonomModel(double coeff, int degree){
        this.coeff=coeff;
        this.degree= degree;
    }

    /**
     * Method for differentiating a monom
     */
    public void derivare(){
        this.coeff = this.coeff*degree;
        this.degree = this.degree- 1;
    }

    /**
     * Method for integrating a monom
     */
    public void integrare(){
        double aux = this.degree+1;
        this.coeff= this.coeff / aux;
       this.degree += 1;

    }

    public void setCoeff(double x){
        this.coeff=x;
    }


    public double getCoeff(){
        return this.coeff;
    }

    public int getDegree(){
        return this.degree;
    }

}
