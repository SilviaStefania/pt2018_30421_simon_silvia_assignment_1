package PT2018.assig1.Model; /** First homework: Operations on polynoms
 *
 *     @author Simon Silvia Stefania, gr 30421
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class PolinomModel {
    /**
     * The class in which are implemented most of the methods that perform operations on polynoms
     * @param mon the main list with the monoms of this polynom; after each operation is performed, the final result that will be displayed is also here
     * @param monOpMul auxiliary list of monoms for this polynom
     * @param monAdunare auxiliary list of monoms for this polynom
     * @param auxpr aux object of type Model.MonomModel to store a monom
     * @param finalString the string that will be printed as output
     * @param rezultat  aux object of type Model.MonomModel to store a polynom
     */

    private List<MonomModel> ptImp;
    private int indexRez;
    private int sizee;
    private List<MonomModel> myList = new ArrayList<MonomModel>();
    private List<MonomModel> mon = new ArrayList<MonomModel>();
    private List<MonomModel> rezultat = new ArrayList<MonomModel>();
    private List<MonomModel> monOpMul = new ArrayList<MonomModel>();
    private List<MonomModel> monAdunare = new ArrayList<MonomModel>();
    private MonomModel auxpr;
    private MonomModel auxaldoilea;
    private StringBuilder finalString = new StringBuilder();

    /**
     * The constructor for the class Polynom that takes a string and constructs its monom list
     * Firstly, the string is split by the + sign and then i consider every part of the string as a monom ;
     * When i iterate in every part of the string, if i cannot find "x" it means that i have found the free term and i add it to the monom with degree 0;
     * else, i split again by "*x^" and i extract the coefficient and the degree
     * @param pol the initial string that it will be entered in the console application
     */

    public PolinomModel(String pol) {

        ptImp=new ArrayList<MonomModel>();
        String delims1 = "[\\+\\ ]+";
        String[] inMon = pol.split(delims1);

        for (String anInMon : inMon) {
            if (anInMon.indexOf('x') < 0) {    //daca nu gasesc x inseamna ca e termenul liber
                mon.add(new MonomModel(Double.parseDouble(anInMon), 0));
            } else {     // if i find x, i have to split the monom to find the coefficient and the degree
                takeCandD(anInMon);
            }
        }

    }

    /**
     * Method used for spliting the monom of form " coeff*x^degree "
     * @param x the string that will be parsed
     */
    private void takeCandD(String x){
        String delims2 = "[x\\ * \\^ ]+"; // luam fiecare monom in parte si il delimitam
        String[] inCandA = x.split(delims2);

        if(x.length() == 1){ //inseamna ca am doar x deci coeff si deg sunt 1
            mon.add(new MonomModel(1,1));

        }
        else if (x.length() >= 5){
            mon.add(new MonomModel(Integer.parseInt(inCandA[0]),Integer.parseInt(inCandA[1])));

        }
        else if (x.length() == 3) { //daca am un singur nr e ori dg ori coff
            if (x.indexOf('*')<0){   //daca nu am * e doar degree cu coeff 1
                mon.add(new MonomModel(1, Integer.parseInt(inCandA[1])));
            }

            if (x.indexOf('^')<0){   //daca nu am ^ e doar coeff cu degree 1
                mon.add(new MonomModel(Double.parseDouble(inCandA[0]), 1));
            }
        }
    }

    /**
     * @param myList2
     * @return  Returns a list
     */
    public List<MonomModel> getList(List<MonomModel> myList2){
        return myList = myList2;
    }

    /**
     * Method used to get the most important list: mon (this is the one used to hold the final string that will be my final answer)
     * @return
     */
    public List<MonomModel> geMtList(){
        return this.mon;
    }


    /**
     * Method used for differentiating a polynom
     * @return the list of monoms that holds the result after differentiating the first polynom
     */
    public List<MonomModel> derivarePol(){
        Iterator<MonomModel> ii =  mon.iterator();
        while (ii.hasNext()){
            auxaldoilea = ii.next();
            auxaldoilea.derivare();
        }
        return this.mon;
    }

    /**
     * Method used for integrate a polynom
     * @return the list of monoms that holds the result after integrating the first polynom
     */
    public List<MonomModel> integrarePol(){
        Iterator<MonomModel> ii =  mon.iterator();
        while (ii.hasNext()){
            auxaldoilea = ii.next();
            auxaldoilea.integrare();
        }
        return this.mon;
    }

    /**
     * Method used for adding two polynoms
     * @param p2 the polynom with which the addition will be made
     * @return the list of monoms that holds the result after differentiating the first polynom
     */
    public List<MonomModel> sum(PolinomModel p2) {
        Iterator<MonomModel> itt = this.mon.iterator();
        while (itt.hasNext()) {
            auxpr = itt.next();
            Iterator<MonomModel> itt2 = p2.mon.iterator();
            while (itt2.hasNext()) {
                auxaldoilea = itt2.next();
                if (auxpr.getDegree() == auxaldoilea.getDegree()) {
                    auxpr.setCoeff(auxpr.getCoeff() + auxaldoilea.getCoeff());
                    monOpMul.add(auxaldoilea); //am toate cele care au fost gasite ca same degree pana acuma
                }

            }
        }
        p2.mon.removeAll(monOpMul); //raman doar cu cele care nu au fost gasite ca same degree
        this.mon.addAll(p2.mon);
        return this.mon;
    }
    /**
     * Method used for multiplying two polynoms
     * @param p2 the polynom with which the multiplication will be made
     * @return the list of monoms that holds the result after multiplying the first polynom
     */
    public List<MonomModel> mul(PolinomModel p2) {
        Iterator<MonomModel> itt = this.mon.iterator();
        while (itt.hasNext()) {
            auxpr = itt.next();
            for (MonomModel aMon : p2.mon) {
                auxaldoilea = aMon;
                monOpMul.add(new MonomModel(auxpr.getCoeff() * auxaldoilea.getCoeff(), auxpr.getDegree() + auxaldoilea.getDegree()));
            }
        }
        this.mon= monOpMul;
        this.monAdunare = this.mon;

        //if in the result i have monos with the same degree, i add the coefficients together
        //iterez prin aceeasi lista (lita in sine si o clona) si cand ajung la same degree & indecsi diferiti, in lista principala fac suma la coeficinti la termenul de la care am plecat si ala la care
        //am ajuns fac 0 ca sa nu l mai afisez
        Iterator<MonomModel> itt2 = this.mon.iterator();
       while(itt2.hasNext()){
            auxpr = itt2.next();
            Iterator<MonomModel> itt6 = this.monOpMul.iterator();
            while(itt6.hasNext()) {
                auxaldoilea = itt6.next();
                if (this.mon.indexOf(auxpr) != this.monOpMul.indexOf(auxaldoilea) && auxaldoilea.getDegree() == auxpr.getDegree() ) {
                    auxpr.setCoeff(auxpr.getCoeff()+auxaldoilea.getCoeff());
                    this.mon.get(this.mon.indexOf(auxaldoilea)).setCoeff(0);
                }
            }
        }
     return this.mon;
    }


    /**
     * Method used for dividing two polynomials
     * @param p2 the polynom with which the division will be made
     * @return the list of monoms that holds the remainder followed by the result after dividing the first polynom
     */
    public List<MonomModel> division(PolinomModel p2){
        Iterator<MonomModel> itr2 = p2.mon.iterator();
        MonomModel test;
        test=itr2.next();
        //first i verify if the division is not done by 0
        if(test.getCoeff() == 0 && test.getDegree()==0){
            return p2.mon;
        }else {
            Iterator<MonomModel> itr = p2.mon.iterator();
            MonomModel auu = new MonomModel(0, 0);
            MonomModel auu2;
            auxaldoilea = null;
            while (itr.hasNext()) {  //make a copy of the divident
                auxaldoilea = itr.next();
                p2.ptImp.add(auxaldoilea);
            }
            int maxDegThis = theGr(this, this.mon).getDegree();
            int maxDegP2 = p2.theGr(p2, p2.mon).getDegree();
            p2.mon.clear();
            while (maxDegThis >= maxDegP2) {
                MonomModel u = new MonomModel(theGr(this, mon).getCoeff() / p2.theGr(p2, p2.ptImp).getCoeff(), theGr(this, mon).getDegree() - p2.theGr(p2, p2.ptImp).getDegree());
                this.rezultat.add(u);
                for (MonomModel aPtImp : p2.ptImp) {
                    auxaldoilea = aPtImp;
                    p2.mon.add(new MonomModel(auxaldoilea.getCoeff() * rezultat.get(this.rezultat.size() - 1).getCoeff(), auxaldoilea.getDegree() + rezultat.get(this.rezultat.size() - 1).getDegree()));
                }

                this.mon = this.diferenta(p2);
                Iterator<MonomModel> itera = this.mon.iterator();
                //if the coefficinet is 0, i remove that term
                while (itera.hasNext()) {
                    auu = itera.next();
                    if (auu.getCoeff() == 0)
                        itera.remove();
                }
                //daca remainderul e 0
                if (this.mon.size() != 0) {
                    maxDegThis = theGr(this, mon).getDegree();
                } else {
                    maxDegThis = -1;
                }
                p2.mon.clear();
            }
            //adaug rezultatul in continuarea restului
            Iterator<MonomModel> iu = this.rezultat.iterator();
            indexRez = this.mon.size(); ///folosit in afisare
            while (iu.hasNext()) {
                auu2 = iu.next();
                this.mon.add(auu2);

            }
            sizee = mon.size(); //folosit in afisare
            return this.mon;
        }
    }


    /**
     * Method used for substracting two polynoms
     * @param p2 the polynom with which the substraction will be made
     * @return the list of monoms that holds the result after substracting the first polynom
     */
    public List<MonomModel> diferenta(PolinomModel p2) {
        for (MonomModel aMon1 : this.mon) {
            auxpr = aMon1;
            for (MonomModel aMon : p2.mon) {
                auxaldoilea = aMon;
                if (auxpr.getDegree() == auxaldoilea.getDegree()) {
                    auxpr.setCoeff(auxpr.getCoeff() - auxaldoilea.getCoeff());
                    monOpMul.add(auxaldoilea); //am toate cele care au fost gasite da degree pana acuma
                }
            }
        }
        p2.mon.removeAll(monOpMul); //raman doar cu cele care nu au fost gasite ca degree
        for (MonomModel aMon : p2.mon) {
            auxaldoilea = aMon;
            auxaldoilea.setCoeff(auxaldoilea.getCoeff() * -1);
        }
        this.mon.addAll(p2.mon);
        return this.mon;
    }


    /**
     * Method that gets the monom with the hightes degree
     * @return monom with the hightes degree
     */
    //am nevoie de cel mai mare monom din lista, termenul dominant
    public MonomModel theGr(PolinomModel p2, List<MonomModel> myList2) {
        Iterator<MonomModel> itt = p2.getList(myList2).iterator();
        MonomModel maxiMon;
        maxiMon = itt.next();
        int maxi = maxiMon.getDegree();
        while (itt.hasNext()){
            auxpr = itt.next();
            if(maxi < auxpr.getDegree()){
                maxi = auxpr.getDegree();
                maxiMon = auxpr;
            }
        }
        return maxiMon;
    }


    /**
     * Method for transformig the list of monoms into a polynom
     * @param afis the list of monoms that will make the final polynom to display
     */
    public String makeString(List<MonomModel> afis) {
        finalString.setLength(0);
        String outputFinal = "";
        if (afis.size() == 0){
            finalString.append("0");
        }
        else {
        Iterator<MonomModel> itp1 = afis.iterator();
        while (itp1.hasNext()){
            auxpr =itp1.next();
            if(auxpr.getCoeff() != 0){
                finalString.append(auxpr.getCoeff());
                if(auxpr.getDegree()!=0) {

                finalString.append("*x^");
                finalString.append(auxpr.getDegree());
                }
            }
            if(afis.indexOf(auxpr) != afis.size()-1 )
            finalString.append("+");
        }

        }
        outputFinal = finalString.toString();

        return outputFinal;

    }
    public void setMyList(List<MonomModel> mon){
        this.mon=mon;
    }

    public int getIndexRez(){
        return this.indexRez;
    }

    public int getSizee(){
        return this.sizee;
    }
}





