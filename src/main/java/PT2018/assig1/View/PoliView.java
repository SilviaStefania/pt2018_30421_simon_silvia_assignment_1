package PT2018.assig1.View;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;


public class PoliView extends JFrame {

    private JTextField firstPoli = new JTextField(15);
    // trebuie puse una sub alta
    private JTextField secondPoli = new JTextField(15);

    private JButton derivateButton = new JButton("Derivate");
    private JButton addButton = new JButton("Addition");
    private JButton subButton = new JButton("Substraction");
    private JButton integrButton = new JButton("Integrate");
    private JButton mulButton = new JButton("Multiplication");
    private JButton divButton = new JButton("Divison");

    private JTextField calculateSol = new JTextField(30);
    private JTextField remainderSol = new JTextField(30);

    //pt pozitionare elemente
    GridBagConstraints aranjare= new GridBagConstraints();

    /**
     * The constructor for initializing the pannel
     */
    public PoliView(){
        JPanel calcPanel =  new JPanel(new GridBagLayout()); //aici adaug toate elementele
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(900, 500);
        this.setTitle("Polynoms Operations");
        calculateSol.setEditable(false);
        remainderSol.setEditable(false);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 0;
        aranjare.gridx = 2;
        JLabel enterFP = new JLabel("Enter first polynom:");
        calcPanel.add(enterFP, aranjare);

        aranjare.insets = new Insets(2, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 2;
        calcPanel.add(firstPoli, aranjare);

        aranjare.gridy = 2;
        aranjare.gridx = 2;
        JLabel enterSP = new JLabel("Enter second polynom:");
        calcPanel.add(enterSP, aranjare);

        aranjare.gridy = 3;
        aranjare.gridx = 2;
        calcPanel.add(secondPoli, aranjare);

        aranjare.insets = new Insets(2, 4, 15, 2);
        aranjare.gridy = 4;
        aranjare.gridx = 0;
        calcPanel.add(derivateButton, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx = 1;
        calcPanel.add(integrButton, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx = 2;
        calcPanel.add(mulButton, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx = 3;
        calcPanel.add(divButton, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx = 4;
        calcPanel.add(addButton, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx = 5;
        calcPanel.add(subButton, aranjare);

        aranjare.insets = new Insets(5, 15, 15, 15);
        aranjare.gridy = 5;
        aranjare.gridx = 2;
        JLabel rez = new JLabel("Your answer:");
        calcPanel.add(rez, aranjare);


        aranjare.gridy = 6;
        aranjare.gridx = 2;
        calcPanel.add(calculateSol, aranjare);

        aranjare.insets = new Insets(5, 15, 15, 15);
        aranjare.gridy = 9;
        aranjare.gridx = 2;
        calcPanel.add(remainderSol, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 11;
        aranjare.gridx = 2;
        JLabel use = new JLabel("Use 1st pol for intr&diff");
        calcPanel.add(use, aranjare);

        this.add(calcPanel);

    }

    /**
     * Get the fisrt polynomial
     * @return
     */
    public String getFisrtPoli(){
        return (firstPoli.getText());
    }
    /**
     * Get the second polynomial
     * @return
     */
    public String getSecondPoli(){
        return (secondPoli.getText());
    }


    /**
     * Set the answer
     * @param solution
     */
    public void setCalculateSol(String solution){
        calculateSol.setText(solution);
    }

    /**
     * Set the remainder after making the division
     * @param solution
     */
    public void setRemainderSol(String solution){
        remainderSol.setText(solution);
    }

    /**
     * Add listener for differentiating button
     * @param listenForDerivateButton
     */
    public void addDerivateListener(ActionListener listenForDerivateButton){
        derivateButton.addActionListener(listenForDerivateButton);
    }

    /**
     * Add listener for integrating button
     * @param listenForIntegrateButton
     */
    public void addIntegrateListener(ActionListener listenForIntegrateButton){
        integrButton.addActionListener(listenForIntegrateButton);
    }

    /**
     * Add listener for multiplication button
     * @param listenFormulButton
     */
    public void addMulListener(ActionListener listenFormulButton){
        mulButton.addActionListener(listenFormulButton);
    }

    /**
     * Add listener for dividing button
     * @param listenForDivButton
     */
    public void addDivListener(ActionListener listenForDivButton){
        divButton.addActionListener(listenForDivButton);
    }

    /**
     * Add listener for adding button
     * @param listenForSumButton
     */
   public void addSumListener(ActionListener listenForSumButton){
       addButton.addActionListener(listenForSumButton);
    }

    /**
     * Add listener for Substraction button
     * @param listenForDiferentaButton
     */
    public void addDiferentaListener(ActionListener listenForDiferentaButton){
        subButton.addActionListener(listenForDiferentaButton);
    }


}
