package PT2018.assig1;
import PT2018.assig1.Model.PolinomModel;
import org.junit.*;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertEquals;

public class PolTest
{
    PolinomModel polynomial1 = new PolinomModel("1*x^3+-2*x^1+1");
    PolinomModel polynomial2 = new PolinomModel("1*x^1+2");

    String addition = "1.0*x^3+-1.0*x^1+3.0" ;
    String substraction = "1.0*x^3+-3.0*x^1+-1.0";
    String multipli = "1.0*x^4+2.0*x^3+-2.0*x^2++-3.0*x^1+2.0";
    String deerivate = "3.0*x^2+-2.0+";
    String integrate = "0.25*x^4+-1.0*x^2+1.0*x^1";
    String division = "-3.0+1.0*x^2+-2.0*x^1+2.0";



   @Test
    public void testPollModelAddition(){
        assertEquals(addition, polynomial1.makeString(polynomial1.sum(polynomial2)));
    }

    @Test
    public void testPollModelSub(){
        assertEquals(substraction, polynomial1.makeString(polynomial1.diferenta(polynomial2)));
    }

    @Test
    public void testPollModelMul(){
        assertEquals(multipli, polynomial1.makeString(polynomial1.mul(polynomial2)));
    }
    @Test
    public void testPollModelIntegrare(){
        assertEquals(integrate, polynomial1.makeString(polynomial1.integrarePol()));
    }
    @Test
    public void testPollModelDerivare(){
        assertEquals(deerivate, polynomial1.makeString(polynomial1.derivarePol()));
    }
    @Test
    public void testPollModelDivision(){
        assertEquals(division, polynomial1.makeString(polynomial1.division(polynomial2)));
    }




}
